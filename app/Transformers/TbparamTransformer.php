<?php

namespace App\Transformers;

use App\Model\TBPARAM;
use League\Fractal\TransformerAbstract;

class TbparamTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TBPARAM $tbparam)
    {
        return [
            'id' => (string) $tbparam->PARAM_CO,
            'description' => (string) $tbparam->PARAM_DESC,
            'value' => (string) $tbparam->PARAM_VA,
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'id' => 'PARAM_CO',
            'description' => 'PARAM_DESC',
            'value' => 'PARAM_VA',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

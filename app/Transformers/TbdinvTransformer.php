<?php

namespace App\Transformers;

use App\Model\TBDINV;
use League\Fractal\TransformerAbstract;

class TbdinvTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TBDINV $tbdinv)
    {
        return [
            'id' => (string) $tbdinv->DINV_SEC,
            'linea' => (string) $tbdinv->DINV_LINEA,
            'producto' => (string) $tbdinv->producto->DESCRIPCION,
            'cantidad' => (int) $tbdinv->DINV_CANT,
            'precio_unitario' => (double) $tbdinv->STOCK1,
            'precio_real' => (double) $tbdinv->DINV_PRECIO_REAL,
            'descuento' => (double) round($tbdinv->DINV_DSC, 2),
            'valor_total' => (double) round($tbdinv->DINV_VTA, 2),
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'id' => 'DINV_SEC',
            'linea' => 'DINV_LINEA',
            'producto' => 'DESCRIPCION',
            'cantidad' => 'DINV_CANT',
            'precio_unitario' => 'STOCK1',
            'precio_real' => 'DINV_PRECIO_REAL',
            'descuento' => 'DINV_DSC',
            'valor_total' => 'DINV_VTA',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

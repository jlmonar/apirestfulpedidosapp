<?php

namespace App\Transformers;

use App\Model\ARBODB;
use League\Fractal\TransformerAbstract;

class ArbodbTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ARBODB $arbodb)
    {
        return [
            'identifier' => (int)$arbodb->NUMERO_ITEM,
            'codigo' => (string)$arbodb->CODIGO,
            'title' => (string)$arbodb->DESCRIPCION,
            'stock' => (double)$arbodb->STOCK_ACTUAL,
            'precioVenta1' => (double)$arbodb->PRECIO_VTA_1,
            'precioVenta2' => (double)$arbodb->PRECIO_VTA_2,
            'precioVenta3' => (double)$arbodb->PRECIO_VTA_3,
            'precioVenta4' => (double)$arbodb->PRECIO_VTA_4,
            'precioVenta5' => (double)$arbodb->PRECIO_VTA_5,
            'precioVenta6' => (double)$arbodb->PRECIO_VTA_6,
            'precioVenta7' => (double)$arbodb->PRECIO_VTA_7,
            'precioVenta8' => (double)$arbodb->PRECIO_VTA_8,
            'precioFOB' => (double)$arbodb->PRECIO_FOB,
            'precioDVenta1' => (double)$arbodb->PRECIOD_VTA_1,
            'precioDVenta2' => (double)$arbodb->PRECIOD_VTA_2,
            'precioDVenta3' => (double)$arbodb->PRECIOD_VTA_3,
            'precioDVenta4' => (double)$arbodb->PRECIOD_VTA_4,
            'precioDVenta5' => (double)$arbodb->PRECIOD_VTA_5,
            'precioDVenta6' => (double)$arbodb->PRECIOD_VTA_6,
            'precioDVenta7' => (double)$arbodb->PRECIOD_VTA_7,
            'precioDVenta8' => (double)$arbodb->PRECIOD_VTA_8,
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'identifier' => 'NUMERO_ITEM',
            'codigo' => 'CODIGO',
            'title' => 'DESCRIPCION',
            'stock' => 'STOCK_ACTUAL',
            'precioVenta1' => 'PRECIO_VTA_1',
            'precioVenta2' => 'PRECIO_VTA_2',
            'precioVenta3' => 'PRECIO_VTA_3',
            'precioVenta4' => 'PRECIO_VTA_4',
            'precioVenta5' => 'PRECIO_VTA_5',
            'precioVenta6' => 'PRECIO_VTA_6',
            'precioVenta7' => 'PRECIO_VTA_7',
            'precioVenta8' => 'PRECIO_VTA_8',
            'precioFOB' => 'PRECIO_FOB',
            'precioDVenta1' => 'PRECIOD_VTA_1',
            'precioDVenta2' => 'PRECIOD_VTA_2',
            'precioDVenta3' => 'PRECIOD_VTA_3',
            'precioDVenta4' => 'PRECIOD_VTA_4',
            'precioDVenta5' => 'PRECIOD_VTA_5',
            'precioDVenta6' => 'PRECIOD_VTA_6',
            'precioDVenta7' => 'PRECIOD_VTA_7',
            'precioDVenta8' => 'PRECIOD_VTA_8',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\Model\TBBIM_TIPOCL;
use League\Fractal\TransformerAbstract;

class TbbimTipoclTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TBBIM_TIPOCL $tbbim_tipocl)
    {
        return [
            'codigo' => (string) $tbbim_tipocl->TIP_CODIGO,
            'description' => (string) $tbbim_tipocl->TIP_DESCRI,
            'status' => (string) $tbbim_tipocl->TIP_STATUS,
            'priceLevel' => (string) $tbbim_tipocl->TIP_NIVELPRE,
            'discount' => (string) $tbbim_tipocl->TIP_PDSCTO,
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'codigo' => 'TIP_CODIGO',
            'description' => 'TIP_DESCRI',
            'status' => 'TIP_STATUS',
            'priceLevel' => 'TIP_NIVELPRE',
            'discount' => 'TIP_PDSCTO',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

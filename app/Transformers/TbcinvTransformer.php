<?php

namespace App\Transformers;

use App\Model\TBCINV;
use League\Fractal\TransformerAbstract;

class TbcinvTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TBCINV $tbcinv)
    {
        return [
            'id' => (int) $tbcinv->CINV_SEC,
            'fecha' => (string) date("d/m/Y", strtotime($tbcinv->CINV_FECING)),
            'total' => (double) round($tbcinv->tbdinvs()->sum('DINV_VTA'), 2),
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'id' => 'CINV_SEC',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

}

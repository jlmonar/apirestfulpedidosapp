<?php

namespace App\Transformers;

use App\Model\ARCLIENTE;
use League\Fractal\TransformerAbstract;

class ArclienteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ARCLIENTE $arcliente)
    {
        return [
            'name' => (string) $arcliente->NOMBRE,
            'lastName' => (string) $arcliente->APELLIDO,
            'businessName' => (string) $arcliente->RAZONS,
            'identification' => (string) $arcliente->CEDULA,
            'rucId' => (string) $arcliente->RUC,
            'address' => (string) $arcliente->DOMICILIO,
            'city' => (string) $arcliente->CIUDAD,
            'phone' => (string) $arcliente->TELEFONOS,
            'email' => (string) $arcliente->MAIL,
            'customerType' => (string) $arcliente->TIPOCLTE,
            'isActive' => ($arcliente->ESTADO === 'Activo'),
            'dcto' => (double) $arcliente->DESCUENTO,
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'name' => 'NOMBRE',
            'lastName' => 'APELLIDO',
            'businessName' => 'RAZONS',
            'identification' => 'CEDULA',
            'rucId' => 'RUC',
            'address' => 'DOMICILIO',
            'city' => 'CIUDAD',
            'phone' => 'TELEFONOS',
            'email' => 'MAIL',
            'customerType' => 'TIPOCLTE',
            'isActive' => 'ESTADO',
            'dcto' => 'DESCUENTO',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

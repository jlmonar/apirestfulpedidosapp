<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TBDINVD extends Model
{
    protected $table = 'TBDINVD';

    protected $primaryKey = 'DINVD_DINV';

    public $timestamps = false;
}

<?php

namespace App\Model;

use App\Transformers\ArbodbTransformer;
use Illuminate\Database\Eloquent\Model;

class ARBODB extends Model
{
    protected $table = 'ARBODB';

    protected $primaryKey = 'NUMERO_ITEM';

    public $timestamps = false;

    public $transformer = ArbodbTransformer::Class;

    protected $hidden = [
        "COSTOD_ACTUAL", "STOCK_INICIAL", "COSTO_ACTUAL", "COSTO_INICIAL", "COSTOD_INICIAL",
        "SECCION", "DATA_S", "GRUPO", "DATA_G", "LINEA", "DATA_L", "UBICACION", "UNIDAD",
        "UNIDAD2", "UNIDAD3", "UNIDAD4", "DATA_U", "MINIMO", "MAXIMO", "FECHA_ULTMOV",
        "FEC_APERTURA", "ESTADO", "FECHA_ULTVEN", "FECHA_STATUS", "FECHA_CORTE", "FECHA_ULTMOD",
        "CODIGO_ITEM_ANT", "TOTAL_VTAS", "TASA_IVA", "TASA_ICE", "MEDIDA", "COMENTARIO",
        "ANO_PROD", "CERTIFICADO", "ULT_PROVEEDOR", "ULT_COMPRA", "ULT_PEDIDO", "ULT_COSTO",
        "ULT_COSTOD", "RBMARGEN_U1", "RBMARGEN_U2", "RBMARGEN_U3", "TIPOINV", "PARTIDA_ARANCELARIA",
        "VALOR_ARANCEL", "TIPO_SERVICIO", "SUBTIPO_INVE_CO", "ITEM_PR_PARAM", "ITEM_PR_PORC1",
        "ITEM_PR_PORC2", "ITEM_PR_PORC3", "ITEM_PR_PORC4", "ITEM_PR_PORC5", "ITEM_PR_PORC6",
        "CENTRO_PROD", "TIPO_RESI_CO", "ITEM_LARGO", "ITEM_ANCHO", "ITEM_ESPESOR", "TIPO_CALC_CO",
        "ITEM_FUELLE_LADO1", "ITEM_FUELLE_LADO2", "ITEM_FUELLE_FONDO", "TIPO_SELLO_TI", "TIPO_SELLO_CO",
        "COLOR_TI", "COLOR_CO", "ITEM_PORCN_PSMINI", "ITEM_PORCN_PSMAXI", "ITEM_SEC_BARRA",
        "UNIDAD5", "UNIDAD6", "UNIDAD7", "UNIDAD8", "FACTOR_VTA", "FACTOR_VTANTERIOR", "FACTOR_COSTO",
        "COSTO_FOB", "PESO", "COSTO_NACIONALIZADO", "DESCPARAR", "ST_COM", "NVL_ETQ", "CAN_DEMANDA",
        "CAN_INCREMENTO", "REFERENCIA", "ST_CODSERIE", "PORC_COMISION", "ST_PIDEPVP", "TRANSITOG",
        "DISPLAY", "EMPAQUE", "CANT1", "CANT2", "CANT3", "CANT4", "CANT5", "CANT6", "CANT7", "CANT8",
        "PART_ARANCEL_COMER", "NOM_PARTIDA_COMER", "PRES1", "PRES2", "PRES3", "PRES4", "PRES5", "PRES6",
        "PRES7", "PRES8", "COD_BAR_ESTADO", "pais", "item_solapa", "des_detallada", "des_corta",
        "volumen_item", "dia_min", "dia_max", "PADRE", "st_RMA", "FACTOR", "PROCE_CODIGO", "PESO_PROD",
        "FACTORP", "CUOTA_INICIAL", "PESOF",
    ];
}

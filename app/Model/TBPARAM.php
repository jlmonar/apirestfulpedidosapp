<?php

namespace App\Model;

use App\Transformers\TbparamTransformer;
use Illuminate\Database\Eloquent\Model;

class TBPARAM extends Model
{
    protected $table = 'TBPARAM';

    protected $primaryKey = 'PARAM_CO';

    public $transformer = TbparamTransformer::class;

    public $timestamps = false;
}

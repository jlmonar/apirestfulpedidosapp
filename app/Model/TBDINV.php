<?php

namespace App\Model;

use App\Transformers\TbdinvTransformer;
use Illuminate\Database\Eloquent\Model;

class TBDINV extends Model
{
    protected $table = 'TBDINV';

    protected $primaryKey = 'DINV_SEC';

    public $transformer = TbdinvTransformer::class;

    public $timestamps = false;

    public function producto()
    {
        return $this->belongsTo('App\Model\ARBODB', 'DINV_ITEM');
    }
}

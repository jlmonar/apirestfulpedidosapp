<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PERMISO_LOTE extends Model
{
    protected $table = 'PERMISO_LOTE';

    protected $primaryKey = 'idPERMISO_LOTE';

    public $timestamps = false;
}

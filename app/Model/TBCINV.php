<?php

namespace App\Model;

use App\Transformers\TbcinvTransformer;
use Illuminate\Database\Eloquent\Model;

class TBCINV extends Model
{
    protected $table = 'TBCINV';

    protected $primaryKey = 'CINV_SEC';

    public $transformer = TbcinvTransformer::class;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function tbdinvs() {
        return $this->hasMany('App\Model\TBDINV', 'DINV_CTINV');
    }
}

<?php

namespace App\Model;

use App\Transformers\ArclienteTransformer;
use Illuminate\Database\Eloquent\Model;

class ARCLIENTE extends Model
{
    protected $table = 'ARCLIENTE';

    protected $primaryKey = 'CODIGO';

    public $transformer = ArclienteTransformer::class;

    protected $hidden = ['FECHA_ULTMOD', 'FEC_APERTURA'];
}

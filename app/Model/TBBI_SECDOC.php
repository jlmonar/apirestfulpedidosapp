<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TBBI_SECDOC extends Model
{
    protected $table = 'TBBI_SECDOC';

    protected $primaryKey = 'DOC_SGL';

    public $timestamps = false;
}

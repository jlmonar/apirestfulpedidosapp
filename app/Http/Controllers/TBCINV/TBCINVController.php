<?php

namespace App\Http\Controllers\TBCINV;

use App\Http\Controllers\APIController;
use App\Model\ARCLIENTE;
use App\Model\PERMISO_LOTE;
use App\Model\TBBI_SECDOC;
use App\Model\TBBIM_TIPOCL;
use App\Model\TBCINV;
use App\Model\TBDINV;
use App\Model\TBDINVD;
use App\Model\TBPARAM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TBCINVController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $bodega_web = env('BODEGA_WEB');
            $ubc_origen = env('UBC_ORIGEN');

            $lote = $this->get_numero_lote($ubc_origen);
            DB::beginTransaction();

            //$cliente = ARCLIENTE::find($request->cliente);
            $cliente = ARCLIENTE::where("CEDULA", $request->cliente)
                ->orWhere("RUC", $request->cliente)
                ->first();
            if (is_null($cliente)) {
                DB::rollBack();
                return $this->errorResponse("No se encontró cliente con la identificacion " .$request->cliente . ".", 404);
            }

            $tipoclte = $cliente->TIPOCLTE;
            if ($tipoclte == '0') {
                $tipoclte = '1';
            }

            //Obtengo nivel de precio del cliente.
            $tipo_cliente = TBBIM_TIPOCL::find($tipoclte);
            $nivel_precio_cliente = $tipo_cliente->TIP_NIVELPRE;
            if (is_null($nivel_precio_cliente)) {
                DB::rollBack();
                return $this->errorResponse("Error al obtener nivel de precio del cliente. ", 404);
            }

            $cinv_sec = TBBI_SECDOC::find('CINV_SEC');
            if (is_null($cinv_sec)) {
                DB::rollBack();
                return $this->errorResponse("Error al obtener secuencia del pedido. ", 404);
            }

            $items_json = $request->items;
            $items_array = json_decode($items_json, TRUE);
            if (is_null($items_array)) {
                DB::rollBack();
                return $this->errorResponse("La cantidad de items del pedido no puede ser cero.", 404);
            }

            $tbcinv = $this->insertTbcinv($cliente, $cinv_sec, $bodega_web, $items_array, $nivel_precio_cliente, $lote, $ubc_origen);
            if (is_string($tbcinv)) {
                DB::rollBack();
                return $this->errorResponse($tbcinv, 404);
            }

            //Actualizo secuencial de la cabecera
            $cinv_sec->DOC_NUM = $cinv_sec->DOC_NUM + 1;
            $cinv_sec->save();

            ########################## Sección donde se inserta detalles del pedido ##########################
            //Parametro Desglosa IVA, si es S se multiplican valores por el IVA 12%, caso contrario queda solo el valor neto.
            $desglosa_iva = TBPARAM::find(1);
            if (is_null($desglosa_iva)) {
                DB::rollBack();
                return $this->errorResponse("Error al obtener parametro deslosa iva. ", 404);
            }

            $dinv_sec = TBBI_SECDOC::find('DINV_SEC');
            if (is_null($dinv_sec)) {
                DB::rollBack();
                return $this->errorResponse("Error al obtener secuencia de los detalles del pedido. ", 404);
            }

            $linea = 1;
            //Inserción de detalles del pedido
            foreach ($items_array as $item) {
                $tbdinv = $this->insertTbdinv($cliente, $dinv_sec, $cinv_sec->DOC_NUM, $item, $linea, $desglosa_iva,$bodega_web, $nivel_precio_cliente);
                if (is_string($tbdinv)) {
                    DB::rollBack();
                    return $this->errorResponse($tbdinv, 404);
                }

                $tbdinvd = $this->insertTbdinvd($cliente, $dinv_sec, $cinv_sec->DOC_NUM, $item, $desglosa_iva);
                if (is_string($tbdinvd)) {
                    DB::rollBack();
                    return $this->errorResponse($tbdinvd, 404);
                }
                //Actualizo secuencial del detalle
                $dinv_sec->DOC_NUM = $dinv_sec->DOC_NUM + 1;
                $dinv_sec->save();

                $linea = $linea + 1;
            }

            ########################## Sección donde se inserta permiso lote ##########################
            $permiso_lote = $this->insertPermisoLote($lote, "TBCINV", 1, $ubc_origen, "001");
            if (is_string($permiso_lote)) {
                DB::rollBack();
                return $this->errorResponse($permiso_lote, 404);
            }
            DB::commit();
            
            //return $this->showMessage("Pedido insertado correctamente", 200);
            return $this->showMessage("true", 200);
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->showMessage("Excepcion ocurrida: " . $ex->getMessage(), 200);
        }
    }

    private function insertTbcinv($cliente, $cinv_sec, $bodega_web, $items_array, $nivel_precio_cliente, $lote, $ubc_origen) {
        try {
            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');
            $fechaActualHoras = $fecha->format('Y-m-d H:i:s');

            $identificacion_usuario = "00";
            if (strlen($cliente->CEDULA) == 10) {
                $identificacion_usuario = $cliente->CEDULA;
            } else if (strlen($cliente->RUC) == 13) {
                $identificacion_usuario = $cliente->RUC;
            }

            $TBCINV = new TBCINV();
            $TBCINV->CINV_SEC = $cinv_sec->DOC_NUM +1;
            $TBCINV->CINV_TDOC = "PV";
            $TBCINV->CINV_NUM = $cinv_sec->DOC_NUM +1;
            $TBCINV->CINV_BOD = $bodega_web;
            $TBCINV->CINV_TBOD = "O";
            $TBCINV->CINV_REF = "000";
            $TBCINV->CINV_FECING = $fechaActual;
            $TBCINV->CINV_ID = $cliente->CODIGO;
            $TBCINV->CINV_NOMID = $cliente->RAZONS;
            $TBCINV->CINV_DSC = 0.0;
            $TBCINV->CINV_COM1 = ".";
            $TBCINV->CINV_COM2 = ".";
            $TBCINV->CINV_COM3 = ".";
            $TBCINV->CINV_COM4 = ".";
            $TBCINV->CINV_TASA = 1.0;
            $TBCINV->CINV_LOGIN = "OPTIMUS WEB";
            $TBCINV->CINV_TDIV = "D";
            $TBCINV->CINV_ST = "A";
            $TBCINV->EMPRESA = "1";
            $TBCINV->DATATR = "N";
            $TBCINV->CODIGOTR = "000";
            $TBCINV->CINV_NOTA = ".";
            $TBCINV->CINV_FPAGO = "C";
            $TBCINV->CINV_HORA = $fechaActualHoras;
            $TBCINV->CINV_TIPRECIO = $nivel_precio_cliente;
            $TBCINV->DATA_EMB = "EM";
            $TBCINV->COD_EMBALADOR = "000";
            $TBCINV->CARTONES = "0";
            $TBCINV->FUNDAS = "0";
            $TBCINV->SACOS = "0";
            $TBCINV->CINV_STPRO = "0";
            $TBCINV->CINV_CEDULA = $identificacion_usuario;
            $TBCINV->CINV_FONOS = $cliente->TELEFONOS;
            $TBCINV->CINV_DIREC = $cliente->DOMICILIO;
            $TBCINV->F_DATA = "F";
            $TBCINV->F_PAGOC = "001";
            $TBCINV->CINV_VENDEDOR = "000";
            $TBCINV->CINV_VDATA = "V";
            $TBCINV->CINV_DIAS = "0";
            $TBCINV->CINV_VCTO1 = ".";
            $TBCINV->CINV_VCTO2 = ".";
            $TBCINV->CINV_VCTO3 = ".";
            $TBCINV->CINV_NUMAPROBACION = "0";
            $TBCINV->CINV_PROBLEMAS = "N";
            $TBCINV->ST_NPC = "1";
            $TBCINV->CINV_ST_EXTERIOR = "0";
            $TBCINV->cinv_fecVencto = $fechaActual;//sin horas
            $TBCINV->FEC_REGISTRO = $fechaActualHoras;//con horas
            $TBCINV->MAQUINA = "ANDROID DEVICE";
            $TBCINV->LMODULO = "0";
            $TBCINV->CINV_ITEMS = count($items_array);
            $TBCINV->cinv_items_sn = "0";
            $TBCINV->CINV_ALMACEN = "1";
            $TBCINV->cinv_campana = "0";
            $TBCINV->cinv_largo_cab = "1";
            $TBCINV->CINV_NAVE_DATA = "NV";
            $TBCINV->CINV_DESTINO_DATA = "PU";
            $TBCINV->CINV_NAVIERA_DATA = "NA";
            $TBCINV->CINV_NAVE = "000";
            $TBCINV->CINV_DESTINOE = "000";
            $TBCINV->CINV_NAVIERA = "000";
            $TBCINV->CINV_BOOKING = "0";
            $TBCINV->UBC_ORIGEN = $ubc_origen;
            $TBCINV->ID_COMUNICACIONES = $lote;
            $TBCINV->save();

            return $TBCINV;
        } catch (\Exception $ex) {
            return "Error al insertar cabecera del pedido em base remota.";
            //return $this->errorResponse("Excepcion ocurrida: " .  $ex->getMessage(), 404);
        }
    }

    private function insertTbdinv($cliente, $dinv_sec, $cinv_sec, $item, $linea, $desglosa_iva, $bodega_web, $nivel_precio_cliente) {
        try {
            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');

            $TBDINV = new TBDINV();
            $TBDINV->DINV_SEC = $dinv_sec->DOC_NUM + 1;
            //$TBDINV->DINV_CTINV = $tbcinv->CINV_SEC;
            $TBDINV->DINV_CTINV = $cinv_sec;
            $TBDINV->DINV_ITEM = $item["numero_item"];
            $TBDINV->DINV_LINEA = $linea;
            $TBDINV->DINV_CANT = $item["quantity"];

            if (strcmp($desglosa_iva->PARAM_VA,"S") == 0) {
                $TBDINV->DINV_DSC = $item["price"] * $item["quantity"] * ($cliente->DESCUENTO/100) / 1.12;
                $TBDINV->DINV_VTA = $item["price"] * $item["quantity"] / 1.12;
                $TBDINV->DINV_COS = $item["price"] * $item["quantity"] / 1.12;
                $TBDINV->DINV_PRECIO_REAL = $item["price"]  / 1.12;

            } else {
                $TBDINV->DINV_DSC = $item["price"] * $item["quantity"] * ($cliente->DESCUENTO/100);
                $TBDINV->DINV_VTA = $item["price"] * $item["quantity"];
                $TBDINV->DINV_COS = $item["price"] * $item["quantity"];
                $TBDINV->DINV_PRECIO_REAL = $item["price"];
            }
            $TBDINV->DINV_IVA = ($TBDINV->DINV_VTA - $TBDINV->DINV_DSC)*0.12;

            $TBDINV->DINV_ICE = 0.0;
            $TBDINV->DINV_PRCT_DSC = $cliente->DESCUENTO;
            $TBDINV->DINV_DSC_EX = 0.0;
            $TBDINV->DINV_BOD = $bodega_web;
            $TBDINV->DINV_TBOD = "O";
            $TBDINV->DINV_FECHA = $fechaActual;
            $TBDINV->DINV_CANT2 = 0.0;
            //$TBDINV->DINV_PRECIO_REAL = $TBDINV->DINV_VTA;
            $TBDINV->EMPRESA = "1";
            $TBDINV->PROMOCION = 0.0;
            $TBDINV->DINV_DCTOPRO = 0.0;
            $TBDINV->DINV_VALPRO = 0.0;
            $TBDINV->DINV_DETALLEDSCTO = "" . round($cliente->DESCUENTO) . "%";
            $TBDINV->DINV_DESCRIPCION = "BODEGA WEB=>" . $item["quantity"];
            $TBDINV->STOCK1 = $item["price"];
            $TBDINV->BODEGA1 = round($cliente->DESCUENTO);
            $TBDINV->CAN_PED = 0;
            $TBDINV->DINV_PRCT_DSC_ANTES = 0.0;
            $TBDINV->DINV_PRECIO_ANTES = 0.0;
            $TBDINV->NIV_PRECIO = $nivel_precio_cliente;
            $TBDINV->DINV_ESTADO = "P";
            $TBDINV->DINV_FALTANTE = 0.0;
            $TBDINV->DINV_REVERSAR = 0.0;
            $TBDINV->dinv_dirent = ".";
            $TBDINV->DINV_CANTAPROBADA = 0.0;
            $TBDINV->DINV_DESC_ITEM = "BODEGA WEB=>" . $item["quantity"];;
            $TBDINV->DINV_DESC_LARGA = 0;
            $TBDINV->DINV_LMODIF = 0;
            $TBDINV->dinv_bulto = 0.0;
            $TBDINV->dinv_largo = 0.0;
            $TBDINV->ST_OBSEQUIO = 0;
            $TBDINV->ST_PRODUCTO = -1;
            $TBDINV->IRBP_DETPED = 0.0;
            $TBDINV->ICE_DETPED = 0.0;

            $TBDINV->save();
        } catch (\Exception $ex) {
            return "Error al insertar detalle del pedido, error en el item: " . $item["name"];
        }
    }

    private function insertTbdinvd($cliente, $dinv_sec, $cinv_sec, $item, $desglosa_iva) {
        try {
            $TBDINVD = new TBDINVD();
            $TBDINVD->DINVD_CTINV = $cinv_sec;
            $TBDINVD->DINVD_TDIV = "D";
            $TBDINVD->DINVD_DINV = $dinv_sec->DOC_NUM + 1;

            if (strcmp($desglosa_iva->PARAM_VA,"S") == 0) {
                $TBDINVD->DINVD_DSC = $item["price"] * $item["quantity"] * ($cliente->DESCUENTO/100) / 1.12;
                $TBDINVD->DINVD_VTA = $item["price"] * $item["quantity"] / 1.12;
                $TBDINVD->DINVD_COS = $item["price"] * $item["quantity"] / 1.12;

            } else {
                $TBDINVD->DINVD_DSC = $item["price"] * $item["quantity"] * ($cliente->DESCUENTO/100);
                $TBDINVD->DINVD_VTA = $item["price"] * $item["quantity"];
                $TBDINVD->DINVD_COS = $item["price"] * $item["quantity"];
            }
            $TBDINVD->DINVD_IVA = ($TBDINVD->DINVD_VTA - $TBDINVD->DINVD_DSC) * 0.12;;
            $TBDINVD->DINVD_ICE = 0.0;
            $TBDINVD->DINVD_DSC_EX = 0.0;
            $TBDINVD->DINVD_PRECIO_REAL = null;
            $TBDINVD->DINVD_VALPRO = 0.0;

            $TBDINVD->save();
        } catch (\Exception $ex) {
            return "Error al insertar detalle del pedidod, error en el item: " . $item["name"]. " error: " . $ex->getMessage();
        }
    }

    private function insertPermisoLote($lote, $tabla, $cantidad, $ubc_origen, $destino) {
        try {
            $fechaCreacion = new DateTime();
            $fechaCreacion = $fechaCreacion->format('Y-m-d H:i:s');
            $rdb_permiso_lote = new PERMISO_LOTE();
            $rdb_permiso_lote->FECHA_SUBIDA = $fechaCreacion;
            $rdb_permiso_lote->TABLA = $tabla;
            $rdb_permiso_lote->CANTIDAD = $cantidad;
            $rdb_permiso_lote->ID_LOTE = $lote;
            $rdb_permiso_lote->ORIGEN = $ubc_origen;
            $rdb_permiso_lote->DESTINO = $destino;
            $rdb_permiso_lote->STATUS = "0";
            if (!is_null($rdb_permiso_lote)) {
                $rdb_permiso_lote->save();
            } else {
                return "Error al insertar permiso lote en base remota. lote: " . $lote;
            }
        } catch(\Exception $ex) {
            return "Error al insertar permiso lote en base remota. lote: " . $lote;
        }
    }

    private function get_numero_lote($ubc_origen) {
        try {
            $numeroLote = $ubc_origen . date('d') . date('m') . date('Y') . date('h') . date('i');
            $aleatorio = rand(1, 100);
            $numeroLote = $numeroLote . $aleatorio;
        } catch (\Exception $e) {
            Log::error("\nArchivo: " . $e->getFile() .
                "\nLínea: " . $e->getLine() .
                "\nBase : Remota" .
                "\nFuncion: get_numero_lote" .
                "\nMensaje: " . $e->getMessage());
            return false;
        }
        return $numeroLote;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getbydate($initial_date, $end_date, $cliente) {
        $initial_date_formatted = date_create_from_format('dmYHis', $initial_date.'000000');
        $end_date_formatted = date_create_from_format('dmYHis', $end_date.'000000');

        if ($initial_date_formatted != null && $end_date_formatted != null) {
            $tbcinvs = TBCINV::whereBetween('CINV_FECING', array($initial_date_formatted, $end_date_formatted))
                ->where('CINV_ID', $cliente)
                ->orderBy('CINV_FECING')
                ->get();
            return $this->showAll($tbcinvs);
        } else {
            return $this->errorResponse("Una de las fechas enviadas como parametro está mal formateada", 404);
        }
    }
}

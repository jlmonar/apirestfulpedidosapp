<?php

namespace App\Http\Controllers\TBBIM_TIPOCL;

use App\Http\Controllers\APIController;
use App\Model\TBBIM_TIPOCL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TBBIM_TIPOCLController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos_cliente = TBBIM_TIPOCL::all();

        return $this->showAll($tipos_cliente);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tipo_id)
    {
        if ($tipo_id == '0') {
            $tipo_id = '1';
        }
        $tipo_cliente = TBBIM_TIPOCL::find($tipo_id);

        if (isset($tipo_cliente))
            return $this->showOne($tipo_cliente);

        return $this->errorResponse("No existe ninguna instancia de tipo_cliente con el id especificado.", 404);
    }
}

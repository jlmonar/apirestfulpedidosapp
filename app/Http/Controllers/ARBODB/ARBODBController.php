<?php

namespace App\Http\Controllers\ARBODB;

use App\Http\Controllers\APIController;
use App\Model\ARBODB;

class ARBODBController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ARBODB::all();

        return $this->showAll($products);
    }

    /**
     * Display the specified resource.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arbodb = ARBODB::find($id);

        if (isset($arbodb))
            return $this->showOne($arbodb);

        return $this->errorResponse("No existe ningún item con el código especificado.", 404);
    }

    public function searchitems($value) {
        $arbodbs = ARBODB::where('DESCRIPCION', 'like', "{$value}%")
        ->where('STOCK_ACTUAL', '>', 0)
        ->get();

        return $this->showAll($arbodbs);
    }
}

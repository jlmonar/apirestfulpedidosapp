<?php

namespace App\Http\Controllers\ARCLIENTE;

use App\Model\ARCLIENTE;
use App\Http\Controllers\APIController;

class ARCLIENTEController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arclientes = ARCLIENTE::all();

        return $this->showAll($arclientes);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ARCLIENTE  $arcliente
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $arcliente = ARCLIENTE::where("CEDULA", $code)
            ->orWhere("RUC",$code)
            ->first();

        if (isset($arcliente))
            return $this->showOne($arcliente);

        return $this->errorResponse("No existe ninguna instancia de cliente con la identificación especificada.", 404);
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('items', 'ARBODB\ARBODBController', ['only' => ['index', 'show']]);
Route::name('searchitems')->get('items/searchitems/{value}', 'ARBODB\ARBODBController@searchitems');

Route::resource('clientes', 'ARCLIENTE\ARCLIENTEController', ['only' => ['index', 'show']]);

Route::resource('pedido', 'TBCINV\TBCINVController', ['only' => ['store']]);
Route::name('getbydate')->get('pedido/getbydate/{initial_date}/{end_date}/{cliente}', 'TBCINV\TBCINVController@getbydate');

Route::resource('detalles_pedido', 'TBDINV\TBDINVController', ['only' => ['show']]);

Route::resource('tipos_clientes', 'TBBIM_TIPOCL\TBBIM_TIPOCLController', ['only' => ['index', 'show']]);

Route::resource('parametros', 'TBPARAM\TBPARAMController', ['only' => ['index', 'show']]);